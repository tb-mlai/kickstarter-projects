#%%
import os
import pandas as pd
import numpy as np
import sklearn as sklearn
from datetime import datetime

from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler, LabelEncoder, OneHotEncoder, LabelBinarizer
from sklearn.model_selection import train_test_split
from IPython.display import display

from sklearn.compose import make_column_transformer

DATA_PATH = './ks-projects-201801.csv'
ATTRIBUTES_TO_DROP = ['name', 'goal', 'state', 'usd pledged', 'pledged', 'ID', 'backers', 'category', 'deadline', 'launched']

def load_data(data_path=DATA_PATH):
    return pd.read_csv(data_path)


#%%
class AttributesAdder(BaseEstimator, TransformerMixin):
    def __init__(self, add_duration=True):
        self.add_duration = add_duration

    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None):
        if self.add_duration:
            launch = X['launched'].values # 2015-08-11 12:12:28
            deadline = X['deadline'].values # 2015-10-09

            display(len(launch), len(deadline))

            launch_dates = [datetime.strptime(launch_date, '%Y-%m-%d %H:%M:%S') for launch_date in launch]
            deadline_dates = [datetime.strptime(deadline_date, '%Y-%m-%d') for deadline_date in deadline]

            durations = [int((x1 - x2).total_seconds()/3600) for (x1, x2) in zip(deadline_dates, launch_dates)]

            # display(durations)
            # display(len(durations))

            return X.assign(duration = durations) 
            # return X
        else:
            return X

class AttributesRemover(BaseEstimator, TransformerMixin):
    def __init__(self, attributes_to_remove):
        self.attributes_to_remove = attributes_to_remove

    def fit(self, X, y=None):
        return self
    
    def transform(self, X, y=None):
        if self.attributes_to_remove:
            return X.drop(labels=self.attributes_to_remove, axis=1)
        else:
            return X


data_clean_pipeline = Pipeline([
    ('attributes_adder', AttributesAdder(add_duration=True)),
    ('attributes_remover_2', AttributesRemover(attributes_to_remove=ATTRIBUTES_TO_DROP))
])

numerical_pipeline = Pipeline([
    ('scaler', StandardScaler())
])

categorical_pipeline = Pipeline([
    ('one_hot_encoder', OneHotEncoder(dtype=np.int, sparse=False))
])

#%%
data = load_data()

#%%
data.head()
data.info()
data.describe()

#%%
train_set, test_set = train_test_split(data, test_size=0.2, random_state=42)
train_labels = train_set['usd_pledged_real']
train_features = train_set.drop('usd_pledged_real', axis=1)
train_features.head()


#%%
train_features_cleaned = data_clean_pipeline.fit_transform(train_features)
train_features_cleaned.head()


#%%
features_numerical = train_features_cleaned[['usd_goal_real', 'duration']]
features_categorical = train_features_cleaned[['main_category', 'currency', 'country']]


#%%
features_numerical_clean = numerical_pipeline.fit_transform(features_numerical)
# data_categorical_clean = categorical_pipeline.fit_transform(data_categorical)
features_categorical_clean = pd.get_dummies(features_categorical, columns=['main_category', 'country', 'currency'], drop_first=True)

display(features_numerical_clean)
display(features_categorical_clean.values)

# display(data_numerical_clean[:1])
# display(data_categorical_clean[:1])


#%%
